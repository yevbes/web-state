package com.yevbes.webstatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Usuario on 10/09/2017.
 */

public class RepositorioDatos {
    private static RepositorioDatos repository = new RepositorioDatos();
    private HashMap<String, CustomList> lists = new HashMap<>();

    public static RepositorioDatos getInstance() {
        return repository;
    }

    private RepositorioDatos() {
        saveLead(new CustomList("Asociación Valenciana de Juristas Democratas", "www.avjd.es", "127.0.0.1", "200", android.R.drawable.btn_star_big_on));
       }

    private void saveLead(CustomList list) {
        lists.put(list.getId(), list);
    }

    public List<CustomList> getLeads() {
        return new ArrayList<>(lists.values());
    }
}
