package com.yevbes.webstatus;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Usuario on 10/09/2017.
 */

public class ListAdapter extends ArrayAdapter<CustomList> {
    public ListAdapter(Context context, List<CustomList> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Obtener inflater.
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // ¿Existe el view actual?
        if (null == convertView) {
            convertView = inflater.inflate(
                    R.layout.custom_list_item,
                    parent,
                    false);
        }

        //mageView imgState = (ImageView) convertView.findViewById(R.id.);
        TextView nameSite = (TextView) convertView.findViewById(R.id.text_view_name_server);
        TextView ipSite = (TextView) convertView.findViewById(R.id.text_view_web_ip);
        TextView urlSite = (TextView) convertView.findViewById(R.id.text_view_web_url);
        TextView code = (TextView) convertView.findViewById(R.id.text_view_status_code);

        // Lead actual.
        CustomList customList = getItem(position);

        // Setup.
        //Glide.with(getContext()).load(customList.getImgId()).into(imgState);
        nameSite.setText(customList.getSiteName());
        ipSite.setText(customList.getIp());
        urlSite.setText(customList.getUrl());
        code.setText(customList.getCode());

        return convertView;
    }
}