package com.yevbes.webstatus;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by yevbes on 09/09/2017.
 */

public class AsyncTaskGetStatusUrl extends AsyncTask<Void,Void,Void> {

    private URL url;
    private HttpURLConnection conn;
    private InetAddress address;
    private final ProgressDialog dialog;
    private String code;
    private String webUrl;
    private String webIP;
    /*private Map<String, List<String>> headerFields;
    private Set<String> headerFieldsSet;
    private Iterator<String> hearerFieldsIter;*/


    AsyncTaskGetStatusUrl(URL url, Context ct){
        this.url = url;
        dialog = new ProgressDialog(ct);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog.setContentView(R.layout.custom_alert_dialog_progress_bar);
        dialog.setTitle("Loading...");
        dialog.show();
    }

    @Override
    protected Void doInBackground(Void... params) {

        try {
            conn = (HttpURLConnection) url.openConnection();
            address = InetAddress.getByName(url.getHost());
            /*headerFields = conn.getHeaderFields();
            headerFieldsSet = headerFields.keySet();
            hearerFieldsIter = headerFieldsSet.iterator();

            while (hearerFieldsIter.hasNext()) {

                String headerFieldKey = hearerFieldsIter.next();
                List<String> headerFieldValue = headerFields.get(headerFieldKey);

                StringBuilder sb = new StringBuilder();
                for (String value : headerFieldValue) {
                    sb.append(value);
                    sb.append("");
                }
                //Log.i("INFO", headerFieldKey + "=" + sb.toString());
            }*/
            code = String.valueOf(conn.getResponseCode());
            webIP = String.valueOf(address.getHostAddress());
            webUrl = String.valueOf(address.getHostName());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        dialog.hide();
    }
}
