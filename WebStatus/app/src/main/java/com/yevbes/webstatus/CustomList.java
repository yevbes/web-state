package com.yevbes.webstatus;

import java.util.UUID;

/**
 * Created by Usuario on 10/09/2017.
 */

public class CustomList {
    private String mId;
    private String siteName;
    private String ip;
    private String url;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    private String code;
    private int imgId;

    public CustomList(String siteName, String url, String ip, String code, int imgId) {
        mId = UUID.randomUUID().toString();
        this.siteName = siteName;
        this.ip = ip;
        this.url = url;
        this.code = code;
        this.imgId = imgId;

    }
    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }


    public String getSiteName() {
        return siteName;
    }

    public String getIp() {
        return ip;
    }

    public int getImgId() {
        return imgId;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "CustomList{" +
                "mId='" + mId + '\'' +
                ", siteName='" + siteName + '\'' +
                ", ip='" + ip + '\'' +
                ", url='" + url + '\'' +
                ", code='" + code + '\'' +
                ", imgId=" + imgId +
                '}';
    }
}
